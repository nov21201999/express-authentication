const mongoose = require('mongoose');

let UserSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: true,
    required: true
  },
  passwordHash: {
    type: String,
    required: true
  },
  verified: {
    type: Boolean,
    required: true,
    default: false
  },
  emailVerificationHash: {
    type: String
  },
  emailVerificationExpiry: {
    type: Number
  },
  passwordResetHash: {
    type: String
  },
  passwordResetExpiry: {
    type: Number
  },
  tempTwoFactorSecret: {
    type: Object
  },
  twoFactorSecret: {
    type: Object
  }

})

module.exports = mongoose.model('User', UserSchema);
