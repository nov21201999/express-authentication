const router = require('express').Router();
const userService = require('../services/user.service');
const middleware = require('../middleware/middleware');

router.get('/', (req, res) => {
  res.render('login');
});

router.get('/register', (req, res) => {
  res.render('register')
});

router.post('/register', (req, res) => {
  userService.RegisterUser(req.body)
    .then((emailVerificationObject) => {
      let message = '(Send this message as an email to the new user) Welcome aboard! Thanks for registering with my amazing app. Please follow this link to confirm your email address: http://localhost:3000/verify-email?email=' + emailVerificationObject.emailAddress + '&emailVerificationString=' + encodeURIComponent(emailVerificationObject.emailVerificationString) + ' (now copy paste this link into a new tab to simulate verifying the email address.)';
      res.render('login', {message: message})
    })
    .catch((error) => {
      res.json({error: 'error creating user'});
    });
});

router.get('/verify-email', (req, res) => {

  userService.VerifyEmailAddress(req.query.email, decodeURIComponent(req.query.emailVerificationString))
    .then((verified) => {
      if (verified === true) {
        res.render('login', {message: 'Email verified! Please log in and enjoy the party.'})
      }
    })
    .catch((error) => {
      console.log('error verifying email address');
      console.log(error);
    })
});

router.post('/login', (req, res) => {
  userService.LoginUser(req, res)
    .then((result) => {
      if (result && result.user) {
        console.log('result');
        console.log(result);
        let user = {
          emailAddress: result.user.email
        }
        res.redirect('members-only?jwtToken=' + result.token)
      } else if (result && result.message) {
        res.render('login', {message: "Error loggin in (either user not found or incorrect password, but we're not telling which because security). Please try again, sign up, or request a new password."});
      } else {
        console.log('result');
        console.log(result);
      }
    })
    .catch((error) => {
      console.log(error);
    })
});

router.get('/members-only', middleware.authenticateUser, (req, res) => {
  userService.GetUser(req.data.email)
    .then((result) => {
      console.log('result from get user in routes')
      console.log(result)
      if (result.message) {
        res.redirect('/login')
      } else {
        res.render('members-only', {user: result, query: req.query})
      }
    })
    .catch((error) => {
      console.log('error');
      console.log(error);
      res.redirect('/login');
    })
});

router.get('/activate-2-factor-authentication', (req, res) => {
  userService.Activate2FactorAuthentication(req.query.email)
    .then((qrCode) => {
      res.render('qrcode', {qrCode: qrCode, email: req.query.email})
    })
    .catch((error) => {
      console.log('error setting 2 factor auth');
      console.log(error);
    })
});

router.post('/verify-two-factor-code', (req, res) => {
  userService.VerifyTwoFactorCode(req.body.twoFactorToken, req.body.email)
    .then((response) => {
      if (response.verified) {
        res.render('members-only', {user: response.user, query: req.query, message: 'Two factor authentication has been set up!!'})
      }
    })
})

router.post('/logout', (req, res) => {
  res.render('logout');
});

router.get('/reset-password', (req, res) => {
  res.render('reset-password');
});

router.post('/reset-password', (req, res) => {
  userService.SendPasswordResetEmail(req.body.email)
    .then((resetCode) => {
      res.render('login', {message: `Show a message like 'If we found that email address then we have sent a password reset email'. Then send an email to the user with this reset link: http://localhost:3000/password-reset?code=${encodeURIComponent(resetCode)}&email=${req.body.email} (Now copy / paste the link to continue the password reset workflow.)`})
    })
});

router.get('/password-reset', (req, res) => {
  userService.CheckPasswordResetCode(decodeURIComponent(req.query.code), req.query.email)
    .then((result) => {
      console.log('result');
      console.log(result);
      if (result.verified) {
        res.render('new-password', {email: req.query.email, code: req.query.code})
      } else {
        res.render('reset-password', {message: result.info})
      }
    })
})


router.post('/new-password', (req, res) => {
  userService.CheckPasswordResetCode(decodeURIComponent(req.body.code), req.body.email)
    .then((result) => {
      if (result.verified) {
        userService.ResetPassword(req.body.email, req.body.password)
          .then((resetUser) => {
            if (!resetUser) {
              console.log('error resetting user password');
            } else {
              res.render('login', {message: "You're password has been reset. Don't forget it next time!"})
            }
          })
      }
    })
})

module.exports = router;
